const mongoose = require('mongoose')
const moment = require('moment')
const request = require('request')

const appointmentSchema = new mongoose.Schema({
    datetime_in: {
        type: String,
        require: true,
        validate(value){
            if(!moment(value,"YYYY-MM-DD LT", true).isValid()){
                throw new Error('Invalid dateTime! ex. 2014-12-13 12:34 PM')
            }
        }
    },
    datetime_out:{
        type: String,
        require: true,
        validate(value){
            if(!moment(value, "YYYY-MM-DD LT", true).isValid()){
                throw new Error('Invalid dateTime! ex. 2014-12-13 12:34 PM')
            }
        }
    },
    report: {
        type: String,
        require: true
    },
    priority: {
        type: Number,
        require: true,
        validate(value){
            if(value < 0 || value > 5) throw new Error('priority must be 1-5')
        }
    },
    createby: {
        type: String,
        require: true,
    }
})

appointmentSchema.methods.timeCheck = async function() {
    const appointment = this
    const datetime_in = appointment['datetime_in']
    const datetime_out = appointment['datetime_out']
    if(datetime_out <= datetime_in) return false
    else return true
}

appointmentSchema.methods.timeCheckPast = async function() {
    // const appointment = this
    // const datetime_in = appointment['datetime_in']
    // const datetime_out = appointment['datetime_out']
    // const url = "http://api.timezonedb.com/v2.1/get-time-zone?key=1S1NYUJP5JXF&format=json&by=zone&zone=Asia/Bangkok";

    // request({ url: url }, (error, response) => {
    //     if (!error && response.statusCode === 200) {
    //         const data = JSON.parse(response.body)
    //         datetime_now = moment(data.formatted).format("YYYY-MM-DD LT")
    //         if(datetime_in > datetime_now || datetime_out > datetime_now) {
    //             console.log(datetime_in + " " +  datetime_now + " " + datetime_out)
    //             // console.log(datetime_in > datetime_now)
    //             // console.log(datetime_out > datetime_now)
    //             return true
    //         }
    //         else{
    //             return false
    //         }
    //     }
    // })
    // return false
}

const appointment = mongoose.model('appointment', appointmentSchema)
module.exports = appointment